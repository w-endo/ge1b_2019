#include "Model.h"
namespace Model
{
	std::vector<ModelData*>	datas;

	int Load(std::string fileNmae, SHADER_TYPE shaderType)
	{
		ModelData* pData = new ModelData;

		pData->fileName = fileNmae;

		pData->shaderType = shaderType;

		for (int i = 0;i < datas.size(); i++)
		{
			if (datas[i]->fileName == fileNmae)
			{
				pData->pFbx = datas[i]->pFbx;
				break;
			}
		}

		if(pData->pFbx == nullptr)
		{
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileNmae);
		}

		datas.push_back(pData);
		return datas.size() -1;
	}

	void SetTransform(int handle, Transform & transform)
	{
		datas[handle]->transform = transform;
	}

	void Draw(int handle)
	{
		datas[handle]->pFbx->Draw(datas[handle]->transform, datas[handle]->shaderType);
	}

	void RayCast(int handle, RayCastData *rayData)
	{
		//�@ レイの通過点を求める
		XMVECTOR target = rayData->start + rayData->dir;

		//�A ワールド行列の逆行列を求める
		XMMATRIX matInv = XMMatrixInverse(nullptr, datas[handle]->transform.GetWorldMatrix());

		//�B rayDataのstartを�Aで変形
		rayData->start = XMVector3TransformCoord(rayData->start, matInv);

		//�C �@を�Aで変形
		target = XMVector3TransformCoord(target, matInv);

		//�D rayDataのdirに�Bから�Cに向かうベクトルを入れる
		rayData->dir = target - rayData->start;

		datas[handle]->pFbx->RayCast(rayData);
	}
}