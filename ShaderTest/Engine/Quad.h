﻿#pragma once
#include "Direct3D.h"
#include <DirectXMath.h>
#include "Texture.h"

using namespace DirectX;


class Transform;

class Quad
{
protected:
	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;
		XMMATRIX	matW;
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;
		XMVECTOR normal;
	};


protected:
	ID3D11Buffer *pVertexBuffer_;
	ID3D11Buffer *pIndexBuffer_;
	ID3D11Buffer *pConstantBuffer_;
	Texture*	pTexture_;
	int indexNum;	//インデックス情報の数

public:
	Quad();
	~Quad();
	virtual HRESULT Initialize();
	void Draw(Transform& transform);
	void Release();
};

