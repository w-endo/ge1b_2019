﻿#pragma once
#include <DirectXMath.h>

using namespace DirectX;

#define vecX  m128_f32[0]
#define vecY  m128_f32[1]
#define vecZ  m128_f32[2]



//位置、向き、拡大率などを管理するクラス
class Transform
{
public:
	XMVECTOR position_;	//位置
	XMVECTOR rotate_;	//向き
	XMVECTOR scale_;	//拡大率
	XMMATRIX matTranslate_;	//移動行列
	XMMATRIX matRotate_;	//回転行列	
	XMMATRIX matScale_;	//拡大行列

	Transform* pParent;

	//コンストラクタ
	Transform();

	//デストラクタ
	~Transform();

	//各行列の計算
	void Calclation();

	//ワールド行列を取得
	XMMATRIX GetWorldMatrix();
};