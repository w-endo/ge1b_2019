﻿#pragma once
#include <DirectXMath.h>
#include "Direct3D.h"
#include "Texture.h"
#include "string"

using namespace DirectX;

class Transform;
class Sprite
{
protected:
	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matW;
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;
	};


protected:
	ID3D11Buffer *pVertexBuffer_;
	ID3D11Buffer *pIndexBuffer_;
	ID3D11Buffer *pConstantBuffer_;
	Texture*	pTexture_;
	int indexNum;	//インデックス情報の数

public:
	Sprite();
	~Sprite();
	virtual HRESULT Initialize(std::string fileName);
	HRESULT Initialize(ID3D11Texture2D*	pTexture);
	void Draw(Transform& transform);
	void Release();
};

