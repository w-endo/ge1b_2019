﻿#pragma once
#include <d3d11.h>

//リンカ
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

enum SHADER_TYPE
{
	SHADER_3D,
	SHADER_2D,
	SHADER_TEST,
	SHADER_TOON,
	SHADER_NORMAL,
	SHADER_WATER,
	SHADER_ENV,
	SHADER_MAX
};

class Texture;

namespace Direct3D
{
	extern ID3D11Device*	pDevice;
	extern ID3D11DeviceContext*    pContext;
	extern UINT winWidth;
	extern UINT winHeight;
	extern Texture*	pToonTex;
	extern Texture*	pCubeTex;

	//初期化
	//引数：winW	ウィンドウの幅
	//引数：winH	ウィンドウの高さ
	//引数：hWnd	ウィンドウハンドル
	//戻値：なし
	void Initialize(int winW, int winH, HWND hWnd);

	//シェーダー準備
	void InitShader();
	void SetShaderBundle(SHADER_TYPE type);
	void InitShader2D();
	void InitShader3D();
	void InitShaderTest();
	void InitShaderToon();
	void InitShaderNormal();
	void InitShaderWater();
	void InitShaderEnv();

	//描画開始
	void BeginDraw();
	void BeginDraw2();

	//描画終了
	void EndDraw();

	//解放
	void Release();
};