#include "Math.h"

//�s��
float Math::Det(XMVECTOR a, XMVECTOR b, XMVECTOR c)
{
	return	(a.vecX * b.vecY * c.vecZ) +
		(a.vecY * b.vecZ * c.vecX) +
		(a.vecZ * b.vecX * c.vecY) -
		(a.vecX * b.vecZ * c.vecY) -
		(a.vecY * b.vecX * c.vecZ) -
		(a.vecZ * b.vecY * c.vecX);
}

bool Math::Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2, float *t)
{
	ray = XMVector3Normalize(ray);

	XMVECTOR edge1 = v1 - v0;
	XMVECTOR edge2 = v2 - v0;

	float denom = Det(edge1, edge2, -ray);

	if (denom < 0)
	{
		return false;
	}

	float u = Det(origin - v0, edge2, -ray) / denom;
	float v = Det(edge1, origin - v0, -ray) / denom;
	*t = Det(edge1, edge2, origin - v0) / denom;

	if (u >= 0 && v >= 0 && (u + v) <= 1 && t >= 0)
	{
		return true;
	}

	return false;
}
