#pragma once

#include <d3d11.h>
#include <fbxsdk.h>
#include <string>
#include "Transform.h"
#include "Direct3D.h"

#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")


//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR	start;	//レイ発射位置
	XMVECTOR	dir;	//レイの向きベクトル
	float		dist;	//衝突点までの距離
	BOOL        	hit;	//レイが当たったか
};


class Texture;

class Fbx
{
	//マテリアル
	struct MATERIAL
	{
		Texture*	pTexture;
		XMFLOAT4	diffuse;
		XMFLOAT4	ambient;
		XMFLOAT4	specular;
		float		shininess;
		Texture*	pNormalTexture;
	};

	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;
		XMMATRIX	matNormal;
		XMMATRIX	matW;
		XMFLOAT4	camPos;
		XMFLOAT4	diffuseColor;
		XMFLOAT4	ambientColor;
		XMFLOAT4	specularColor;
		float		shininess;
		BOOL		isTexture;
		float		scroll;
	};

	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;
		XMVECTOR normal;
		XMVECTOR tangent;
	};

	int vertexCount_;	//頂点数
	int polygonCount_;	//ポリゴン数
	int materialCount_;	//マテリアルの個数


	ID3D11Buffer *pVertexBuffer_;
	ID3D11Buffer **pIndexBuffer_;
	ID3D11Buffer *pConstantBuffer_;
	MATERIAL	 *pMaterialList_;
	int			 *indexCountEachMaterial_;

	VERTEX* pVertices_;
	int** ppIndex_;

	void InitVertex(fbxsdk::FbxMesh * mesh);
	void InitIndex(fbxsdk::FbxMesh * mesh);
	void IntConstantBuffer();
	void InitMaterial(fbxsdk::FbxNode* pNode);

public:

	Fbx();
	HRESULT Load(std::string fileName);
	void    Draw(Transform& transform, SHADER_TYPE shaderType);
	void    Release();
	void	RayCast(RayCastData* rayData);
};

