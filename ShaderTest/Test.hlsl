//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D	g_texture : register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー


//───────────────────────────────────────
// コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	float4x4	matWVP;			// ワールド・ビュー・プロジェクションの合成行列
	float4x4	matNormal;		//回転×拡大の逆行列
	float4x4	matW;			//ワールド行列
	float4		camPos;			//視点（カメラの位置）
	float4		diffuseColor;	// 拡散反射光
	float4		ambientColor;	// 環境光
	float4		specularColor;	// 鏡面反射光
	float		shininess;		//光沢度
	bool		isTexture;		// テクスチャ貼ってあるかどうか
};

struct VS_OUT
{
	float4 pos : SV_POSITION;
	float4 normal : TEXCOORD1;
	float4 eye	: TEXCOORD2;
	float2 uv	: TEXCOORD;	
};



//頂点シェーダー
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL )
{
	VS_OUT outData;
	outData.pos = mul(pos, matWVP);

	outData.uv = uv;


	normal.w = 0;

	outData.normal = mul(normal, matNormal);

	outData.eye = normalize( camPos - mul(pos, matW));	//視点ベクトル

	return outData;
}

//ピクセルシェーダー
float4 PS(VS_OUT inData) : SV_TARGET
{
	float4 light = float4(1, -1, 1, 0);
	light = normalize(light);

	float4 ambient = ambientColor; //環境光（ka*ia）
	float4 LN = saturate(dot(-light, normalize(inData.normal)));

	float4 id;
	if (isTexture == true)
	{
		id = g_texture.Sample(g_sampler, inData.uv);
	}
	else
	{
		id = diffuseColor;
	}
	
	
	float4 diffuse = LN * id;		//拡散反射光
	float4 R = reflect(light, normalize(inData.normal));	//反射ベクトル
	float ks = 1;	//鏡面反射係数（ハイライトの強さ）
	float a =  shininess;	//光沢度（ハイライトの大きさ）
	float speculer = ks * pow(saturate(dot(R, normalize(inData.eye))), a) * specularColor;	//鏡面反射光

	float4 color = ambient + diffuse +speculer;

	return color;
}