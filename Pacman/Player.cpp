﻿#include "Player.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), hModel_(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);

	pStage_ = (Stage*)FindObject("Stage");
	assert(pStage_ != nullptr);

	transform_.position_.vecX = 5;
	transform_.position_.vecZ = 5;
}

//更新
void Player::Update()
{
	XMVECTOR prevPosition = transform_.position_;


	XMVECTOR move = XMVectorSet( 0, 0, 0, 0 );

	if (Input::IsKey(DIK_RIGHT) || Input::IsPadButton(XINPUT_GAMEPAD_DPAD_RIGHT))
	{
		move.vecX = 1;
	}

	if (Input::IsKey(DIK_LEFT))
	{
		move.vecX = -1;
	}

	if (Input::IsKey(DIK_UP))
	{
		move.vecZ = 1;
	}

	if (Input::IsKey(DIK_DOWN))
	{
		move.vecZ = -1;
	}

	move.vecX = Input::GetPadStickL().vecX;
	move.vecZ = Input::GetPadStickL().vecY;

	

	//移動してる
	if (XMVector3Length(move).vecX > 0)
	{
		//移動
		move = XMVector3Normalize(move);
		transform_.position_ += move * 0.2f;

		//向きを変える
		//デフォルトの向き
		XMVECTOR front = XMVectorSet(0, 0, 1, 0);

		//角度求めて
		float dot = XMVector3Dot(front, move).vecX;//内積を求める
		float angle = acos(dot);            //acosを求めれば角度

		//向き求めて
		XMVECTOR cross = XMVector3Cross(front, move);
		if (cross.vecY < 0)
		{
			angle = -angle;	//左向きだから角度反転
		}

		//回転
		transform_.rotate_.vecY = XMConvertToDegrees(angle);
	}

	int checkX, checkZ;

	//左
	checkX = (int)(transform_.position_.vecX - 0.3f);
	checkZ = (int)transform_.position_.vecZ;
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecX = checkX + 1 + 0.3f;
	}

	//右
	checkX = (int)(transform_.position_.vecX + 0.3f);
	checkZ = (int)transform_.position_.vecZ;
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecX = checkX - 0.3f;
	}

	//奥
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ + 0.3f);
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecZ = checkZ - 0.3f;
	}

	//前
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ - 0.3f);
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecZ = checkZ + 1 + 0.3f;
	}

}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}