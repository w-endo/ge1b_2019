﻿#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/CsvReader.h"
#include "Engine/Camera.h"

//コンストラクタ
Stage::Stage(GameObject * parent)
	:GameObject(parent, "Stage")
{
	for (int i = 0; i < 2; i++)
	{
		hModel_[i] = -1;
	}
	ZeroMemory(table_, sizeof(table_));

	CsvReader csv;
	csv.Load("Map.csv");

	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			table_[x][z] = csv.GetValue(x, z);
		}
	}


	Camera::SetPosition(XMVectorSet(7.5, 7, -7, 0));
	Camera::SetTarget(XMVectorSet(7.5, 0, 6.5, 0));

}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//モデルデータのロード
	hModel_[1] = Model::Load("Wall.fbx");
	assert(hModel_[1] >= 0);

	hModel_[0] = Model::Load("Floor.fbx");
	assert(hModel_[0] >= 0);
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			int type = table_[x][z];//床or壁

			Transform trans;
			trans.position_.vecX = x;
			trans.position_.vecZ = z;
			Model::SetTransform(hModel_[type], trans);
			Model::Draw(hModel_[type]);
		}
	}
}

//開放
void Stage::Release()
{
}

//そこは壁？
bool Stage::IsWall(int x, int z)
{
	return table_[x][z] == 1;
}
