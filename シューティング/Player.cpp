#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Bullet.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), hModel_(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);
}

//更新
void Player::Update()
{
	//スペースキーが押されたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		Bullet* pBullet = Instantiate<Bullet>(GetParent());
		pBullet->SetPosition(transform_.position_);
	}



	//右キーが押されたら
	if (Input::IsKey(DIK_RIGHT))
	{
		//右に移動
		transform_.position_.vecX += 0.1f;
	}

	//左キーが押されたら
	if (Input::IsKey(DIK_LEFT))
	{
		//左に移動
		transform_.position_.vecX -= 0.1f;
	}

	//敵がもういない！！
	if (FindObject("Enemy") == nullptr)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);
	}



}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}