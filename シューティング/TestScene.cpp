#include "TestScene.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

//コンストラクタ
TestScene::TestScene(GameObject * parent)
	: GameObject(parent, "TestScene"), hModel_(-1)
{

}

//初期化
void TestScene::Initialize()
{
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);

	transform_.position_.vecX = -5;
}

//更新
void TestScene::Update()
{
	//右キーが押されたら
	if (Input::IsKey(DIK_RIGHT))
	{
		//右に移動
		transform_.position_.vecX += 0.1f;
	}

	//左キーが押されたら
	if (Input::IsKey(DIK_LEFT))
	{
		//左に移動
		transform_.position_.vecX -= 0.1f;
	}
}

//描画
void TestScene::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TestScene::Release()
{
}
