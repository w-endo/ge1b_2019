﻿#include "Enemy.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy"), hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Enemy.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = rand() % 21 + (-10);
	transform_.position_.vecZ = 50.0f;

	//衝突範囲
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, -0.3f, 0, 0), 2.0f);
	AddCollider(collision);
}

//更新
void Enemy::Update()
{

}

//描画
void Enemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

//何かに当たった
void Enemy::OnCollision(GameObject * pTarget)
{
	if (pTarget->GetObjectName() == "Bullet")
	{
		KillMe();
		pTarget->KillMe();
	}
}