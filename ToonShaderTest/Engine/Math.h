#pragma once
#include "Transform.h"

namespace  Math
{
	float Det(XMVECTOR a, XMVECTOR b, XMVECTOR c);
	bool Intersect(XMVECTOR start, XMVECTOR Dir, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2, float *t);
};

