#include "Bullet.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
Bullet::Bullet(GameObject * parent)
	:GameObject(parent, "Bullet"), 
	hModel_(-1), move_({ 0, 0, 0, 0 }),
	SPEED(0.5f), GRAVITY(0.03f)
{

}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);


	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void Bullet::Update()
{
	//弾が移動する
	transform_.position_ += move_;
	move_.vecY -= GRAVITY;

	if (transform_.position_.vecY < -10)
	{
		KillMe();
	}
}

//描画
void Bullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

//発射
void Bullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	transform_.position_ = position;
	move_ = XMVector3Normalize(direction) * SPEED;
}
