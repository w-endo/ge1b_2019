#include "Model.h"
namespace Model
{
	std::vector<ModelData*>	datas;

	int Load(std::string fileNmae)
	{
		ModelData* pData = new ModelData;

		pData->fileName = fileNmae;

		for (int i = 0;i < datas.size(); i++)
		{
			if (datas[i]->fileName == fileNmae)
			{
				pData->pFbx = datas[i]->pFbx;
				break;
			}
		}

		if(pData->pFbx == nullptr)
		{
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileNmae);
		}

		datas.push_back(pData);
		return datas.size() -1;
	}

	void SetTransform(int handle, Transform & transform)
	{
		datas[handle]->transform = transform;
	}

	void Draw(int handle)
	{
		datas[handle]->pFbx->Draw(datas[handle]->transform);
	}
}