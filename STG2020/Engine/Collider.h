#pragma once
#include <DirectXMath.h>
using namespace DirectX;

class GameObject;
class Collider
{
	XMVECTOR	center_;
	float		radius_;
	GameObject*	pMaster_;	

public:
	Collider(XMVECTOR center, float radius);
	bool IsHit(Collider* pTarget);
	void SetMaster(GameObject*	pMaster)
	{
		pMaster_ = pMaster;
	}
};

