#include "Enemy.h"
#include "Engine/Model.h"
#include "Engine/Collider.h"

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy")
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets\\Oden.fbx");
	assert(hModel_ >= 0);

	transform_.position_.vecZ = 10.0f;
	transform_.position_.vecX = -5.0f;


	transform_.scale_.vecX = 0.3f;
	transform_.scale_.vecY = 0.3f;
	transform_.scale_.vecZ = 0.3f;

	Collider* collision = new Collider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void Enemy::Update()
{
}

//描画
void Enemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

void Enemy::OnCollision(GameObject * pTarget)
{
	KillMe();
}
