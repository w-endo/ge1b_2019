#include "Player.h"
#include "Engine/Input.h"
#include "Bullet.h"
#include "Engine/Model.h"
#include "MiniOden.h"

//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), hModel_(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{

	//モデルデータのロード
	hModel_ = Model::Load("Assets\\Oden.fbx");
	assert(hModel_ >= 0);

	transform_.scale_.vecX = 0.3f;
	transform_.scale_.vecY = 0.3f;
	transform_.scale_.vecZ = 0.3f;

	Instantiate<MiniOden>(this);
}

//更新
void Player::Update()
{
	transform_.rotate_.vecY++;

	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.position_.vecX += 0.2f;
	}
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.position_.vecX -= 0.2f;
	}

	if (Input::IsKeyDown(DIK_SPACE))
	{
		GameObject* pBullet = Instantiate<Bullet>(pParent_);
		pBullet->transform_.position_ = transform_.position_;

	}
}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}