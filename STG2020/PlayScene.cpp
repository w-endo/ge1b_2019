#include "PlayScene.h"
#include "Player.h"
#include "Engine/Camera.h"
#include "Bullet.h"
#include "Enemy.h"

//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	Instantiate<Player>(this);

	Camera::SetPosition(0, 3, -10);
	Camera::SetTarget(0, 1, 0);

	Instantiate<Bullet>(this);
	Instantiate<Enemy>(this);
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}