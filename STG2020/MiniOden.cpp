#include "MiniOden.h"

#include "Engine/Model.h"

//コンストラクタ
MiniOden::MiniOden(GameObject * parent)
	:GameObject(parent, "MiniOden"), hModel_(-1)
{
}

//デストラクタ
MiniOden::~MiniOden()
{
}

//初期化
void MiniOden::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets\\Oden.fbx");
	assert(hModel_ >= 0);

	transform_.scale_.vecX = 0.1f;
	transform_.scale_.vecY = 0.1f;
	transform_.scale_.vecZ = 0.1f;
	transform_.position_.vecX = 1.0f;
}

//更新
void MiniOden::Update()
{
}

//描画
void MiniOden::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void MiniOden::Release()
{
}