#include <string>

namespace Audio
{
	void Initialize();
	int Load(std::string fileName, int svNum = 1);
	void Play(int ID);
	void Release();
}