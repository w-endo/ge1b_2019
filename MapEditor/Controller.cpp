#include "Controller.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"

//コンストラクタ
Controller::Controller(GameObject * parent)
	:GameObject(parent, "Controller")
{
	transform_.position_.vecX = 7.0f;
	transform_.position_.vecZ = 7.0f;
	transform_.rotate_.vecX = 45.0f;
}

//デストラクタ
Controller::~Controller()
{
}

//初期化
void Controller::Initialize()
{
}

//更新
void Controller::Update()
{
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += 1.0f;
	}
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= 1.0f;
	}
	if (Input::IsKey(DIK_UP))
	{
		transform_.rotate_.vecX += 1.0f;
		if (transform_.rotate_.vecX >= 90)
		{
			transform_.rotate_.vecX = 89;
		}
	}
	if (Input::IsKey(DIK_DOWN))
	{
		transform_.rotate_.vecX -= 1.0f;
		if (transform_.rotate_.vecX < 0)
		{
			transform_.rotate_.vecX = 0;
		}
	}
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));

	XMVECTOR right{ 0.1f, 0, 0, 0 };
	XMVECTOR front{ 0, 0, 0.1f, 0 };
	right = XMVector3TransformCoord(right, matY);
	front = XMVector3TransformCoord(front, matY);
	if (Input::IsKey(DIK_D))
	{
		transform_.position_ += right;
	}
	if (Input::IsKey(DIK_A))
	{
		transform_.position_ -= right;
	}
	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += front;
	}
	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= front;
	}

	XMVECTOR camArm{ 0, 0, -10, 0 };
	camArm = XMVector3TransformCoord(camArm, matX * matY);
	Camera::SetPosition(transform_.position_ + camArm);
	Camera::SetTarget(transform_.position_);

}

//描画
void Controller::Draw()
{
}

//開放
void Controller::Release()
{
}