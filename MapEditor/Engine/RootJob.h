#pragma once
#include "GameObject.h"
class RootJob :	public GameObject
{
public:
	RootJob();
	~RootJob();

	// GameObject を介して継承されました
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};


