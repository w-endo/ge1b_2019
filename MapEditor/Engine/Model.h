#pragma once

#include <string>
#include <vector>
#include "Transform.h"
#include "Fbx.h"

namespace Model
{
	struct ModelData
	{
		Transform transform;
		std::string fileName;
		Fbx* pFbx;

		ModelData() : pFbx(nullptr)
		{}
	};

	


	int Load(std::string fileNmae);
	void SetTransform(int handle, Transform& transform);
	void Draw(int handle);
	void RayCast(int handle, RayCastData *rayData);
};

