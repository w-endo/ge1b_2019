#include "Map.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Direct3D.h"
#include "Engine/Camera.h"
#include "string"

//コンストラクタ
Map::Map(GameObject* parent)
	:GameObject(parent, "Map")
{
	for (int i = 0; i < 5; i++)
	{
		hModel_[i] = -1;
	}

	ZeroMemory(table_, sizeof(table_));

	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			table_[x][z].height = 1;
		}
	}
	table_[2][3].type = TYPE_BRK;
	table_[5][5].type = TYPE_GRS;
	table_[8][4].type = TYPE_WTR;
	table_[2][2].height = 3;
	table_[5][5].height = 4;
}

//デストラクタ
Map::~Map()
{
}

//初期化
void Map::Initialize()
{
	//モデルデータのロード
	std::string fileName[] = {
		"assets\\BoxDefault.fbx" ,
		"assets\\BoxBrick.fbx" ,
		"assets\\BoxGrass.fbx",
		"assets\\BoxSand.fbx",
		"assets\\BoxWater.fbx"
	};

	for (int i = 0; i < TYPE_MAX; i++)
	{
		hModel_[i] = Model::Load(fileName[i].c_str());
		assert(hModel_[i] >= 0);
	}

	RayCastData data;
	data.start = XMVectorSet(1, 10, 0, 0);
	data.dir = XMVectorSet(0, -1, 0, 0);
	Model::RayCast(hModel_[0], &data);



}

//更新
void Map::Update()
{
	//マウスをクリックしたら
	if (Input::IsMouseButtonDown(0))
	{
		//ビューポート行列
		float w = Direct3D::winWidth / 2.0f;
		float h = Direct3D::winHeight / 2.0f;
		XMMATRIX vp = {
			w, 0, 0, 0,
			0, -h, 0, 0,
			0, 0, 1, 0,
			w, h, 0, 1
		};

		//各逆行列
		XMMATRIX invVP = XMMatrixInverse(nullptr, vp);
		XMMATRIX invPrj = XMMatrixInverse(nullptr, Camera::GetProjectionMatrix());
		XMMATRIX invView = XMMatrixInverse(nullptr, Camera::GetViewMatrix());


		//クリック位置（手前）
		XMVECTOR mousePosFront = Input::GetMousePosition();
		mousePosFront.vecZ = 0;

		//クリック位置（奥）
		XMVECTOR mousePosBack = Input::GetMousePosition();
		mousePosBack.vecZ = 1;

		//変換
		mousePosFront = XMVector3TransformCoord(mousePosFront, invVP * invPrj * invView);
		mousePosBack = XMVector3TransformCoord(mousePosBack, invVP * invPrj * invView);


		int nearX;
		int nearZ;
		float miniDist = 9999;
		float isHit = false;

		for (int x = 0; x < 15; x++)
		{
			for (int z = 0; z < 15; z++)
			{
				for (int y = 0; y < table_[x][z].height; y++)
				{
					Transform trans;
					trans.position_.vecX = x;
					trans.position_.vecY = y;
					trans.position_.vecZ = z;
					trans.Calclation();

					//レイキャスト
					RayCastData data;
					data.start = mousePosFront;
					data.dir = mousePosBack - mousePosFront;
					Model::SetTransform(hModel_[0], trans);
					Model::RayCast(hModel_[0], &data);

					//当たったかテスト
					if (data.hit)
					{
						isHit = true;
						if (data.dist < miniDist)
						{
							miniDist = data.dist;
							nearX = x;
							nearZ = z;
						}
					}
				}
			}
		}
		if (isHit)
		{
			table_[nearX][nearZ].height++;
		}
	}
}
//描画
void Map::Draw()
{
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			for (int y = 0; y < table_[x][z].height; y++)
			{
				Transform trans;
				trans.position_.vecX = x;
				trans.position_.vecY = y;
				trans.position_.vecZ = z;
				trans.Calclation();
				int type = table_[x][z].type;
				Model::SetTransform(hModel_[type], trans);
				Model::Draw(hModel_[type]);
			}
		}
	}
}

//開放
void Map::Release()
{
}