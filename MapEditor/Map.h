#pragma once
#include "Engine/GameObject.h"

//◆◆◆を管理するクラス
class Map : public GameObject
{
	enum Type
	{
		TYPE_DEF,	//デフォルト
		TYPE_BRK,	//レンガ
		TYPE_GRS,	//草
		TYPE_SND,	//砂
		TYPE_WTR,	//水
		TYPE_MAX
	};



    int hModel_[TYPE_MAX];    //モデル番号

	struct
	{
		int height;	//高さ
		Type type;	//種類
	} table_[15][15];


public:
    //コンストラクタ
    Map(GameObject* parent);

    //デストラクタ
    ~Map();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;
};